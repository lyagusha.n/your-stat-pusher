PROJECT = your_stat_pusher
PROJECT_DESCRIPTION = New project
PROJECT_VERSION = 0.1.0

LOCAL_DEPS = inets
DEPS = lager jsx wolfmq

dep_wolfmq    = git https://github.com/erlangbureau/wolfmq.git    0.5.1

ERLC_OPTS += -smp +'{parse_transform, lager_transform}' +debug_info

include erlang.mk
