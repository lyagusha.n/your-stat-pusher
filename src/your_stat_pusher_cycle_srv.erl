-module(your_stat_pusher_cycle_srv).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-define(ETS, your_stat_pusher_timetable).

%% API
start_link() ->
    gen_server:start_link(?MODULE, null, []).

%% gen_server
init(_) ->
    _ = ets:new(?ETS, [named_table, ordered_set, public, {write_concurrency, true}]),
    Tasks = application:get_env(your_stat_pusher, tasks, []),
    _ = [ok = start_task(T)||T <- Tasks],
    _ = self()!general_timer,
    Interval = 500,
    State = #{interval => Interval},
    {ok, State}.

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(general_timer, State) ->
    TRef     = maps:get(general_timer, State, undefined),
    Interval = maps:get(interval, State),
    _ = case TRef of
        undefined ->
            ignore;
        _ ->
            erlang:cancel_timer(TRef)
    end,
    ok = start_tasks(),
    TRef1 = erlang:send_after(Interval, self(), general_timer),
    NewState = State#{general_timer => TRef1},
    {noreply, NewState};

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% internal
start_tasks() ->
    Key = ets:first(?ETS),
    case Key of
        {Date, _} ->
            case Date =< erlang:system_time(milli_seconds) of
                true ->
                    _ = [start_task(Task)||{{_, Task}} <- ets:take(?ETS, Key)],
                    start_tasks();
                false ->
                    ok
            end;
        '$end_of_table' ->
            ok
    end.

start_task({Module, Function, Arguments, Period, Link} = Task) ->
    Fun = fun() ->
        _ = try
            Json = jsx:encode(erlang:apply(Module, Function, Arguments)),
            Req = {Link, [], "application/x-www-form-urlencoded", Json},
            case httpc:request(post, Req, [], [{body_format, binary}]) of
                {ok, {{_, 204, _}, _, _Response}} ->
                    ignore;
                {ok, {{_, 202, _}, _, _Response}} ->
                    ignore;
                {ok, {{_, 200, _}, _, _Response}} ->
                    ignore;
                Error ->
                    ErrorFormat = "Push data to YourStat error: ~p",
                    ok = lager:error(ErrorFormat, [Error])
            end
        catch
            T:E:S ->
                EFormat = "Push data to YourStat error: ~p",
                ok = lager:error(EFormat, [{T,E,S}])
        end,
        NextApplyDate = erlang:system_time(milli_seconds) + Period,
        true = ets:insert(?ETS, {{NextApplyDate, Task}}),
        ok
    end,
    ok = wolfmq:push(Task, Fun).
