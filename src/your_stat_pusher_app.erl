-module(your_stat_pusher_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
    {ok, _} = application:ensure_all_started(ssl),
    {ok, _} = application:ensure_all_started(inets),
	your_stat_pusher_sup:start_link().

stop(_State) ->
	ok.
