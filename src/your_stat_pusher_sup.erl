-module(your_stat_pusher_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    Procs = [#{
        id => your_stat_pusher_cycle_srv,
        start => {your_stat_pusher_cycle_srv, start_link, []},
        restart => permanent,
        shutdown => brutal_kill,
        type => worker,
        modules => [your_stat_pusher_cycle_srv]
    }],
    SupFlags = #{
        intensity => 5,
        period => 1
    },
    {ok, {SupFlags, Procs}}.
